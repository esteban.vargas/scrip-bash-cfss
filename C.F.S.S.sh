#!/bin/bash

NOMBRE=$1
DIRECTORY=$2

if [ -d "$DIRECTORY" ]; then
	tar -cvzf $1.tar.gz $2
	NOMBRE=$1.tar.gz
	DIRECTORY=$NOMBRE
fi

gpg --sign --armor $DIRECTORY

openssl ts -query -data $DIRECTORY -cert -no_nonce -out request.tsq

cat request.tsq | curl -s -S -H 'Content-Type: application/timestamp-query' --data-binary @- http://tsa.sinpe.fi.cr/tsaHttp/ -o $1.tsr

rm -f request.tsq

openssl sha1 -out $NOMBRE.sha1 $DIRECTORY
